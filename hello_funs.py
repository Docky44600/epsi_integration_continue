"""
Ce module contient des fonctions pour saluer quelqu'un de manière spécifique.
"""

def say_something_to_someone(something, someone):
    """
    Affiche un message spécifique pour quelqu'un.
    
    Args:
        something (str): Le message à afficher.
        someone (str): La personne à saluer.
    """
    return something + " " + someone

def say_hello_world():
    """
    Fonction pour dire 'hello world' à une  personne en particulier.
    """
    return say_something_to_someone("hello world", "")

def say_happy_new_year(nom):
    """
    Fonction pour souhaiter un joyeux anniversaire à quelqu'un en particulier.
    
    Args:
        nom (str): Le nom de la personne à saluer.
    """
    return say_something_to_someone("happy birthday", nom)
